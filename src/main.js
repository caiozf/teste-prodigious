import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import firebase from 'firebase'
import VueTheMask from 'vue-the-mask'
import './registerServiceWorker'

//libs
import VueSweetalert2 from 'vue-sweetalert2';

Vue.use(VueSweetalert2);
Vue.use(VueTheMask);

Vue.config.productionTip = false

let app = null

firebase.auth().onAuthStateChanged(()=> {
	if(!app){
		app = new Vue({
			  router,
			  store,
			  render: h => h(App)
			}).$mount('#app')
	}
})
