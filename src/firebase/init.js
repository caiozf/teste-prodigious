import firebase from 'firebase'

 var config = {
    apiKey: "AIzaSyDBRCThcCupDrNAi30pkfVvt_3W7QWRB8M",
    authDomain: "teste-prodigious.firebaseapp.com",
    databaseURL: "https://teste-prodigious.firebaseio.com",
    projectId: "teste-prodigious",
    storageBucket: "teste-prodigious.appspot.com",
    messagingSenderId: "854310745549"
};

const firebaseApp = firebase.initializeApp(config);

export default firebaseApp;
