import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'
import firebase from 'firebase'
import db from '@/firebase/init'
import VueSweetalert2 from 'vue-sweetalert2';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {
  	login({commit}, payload){
  		
		firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
				.then(()=> {
					Vue.swal({
						toast: true,
						position: 'bottom-end',
						showConfirmButton: false,
	  					timer: 4000,
	  					type: 'success',
	  					title: 'Login efetuado com sucesso'
					});
					router.push({"name": "List"})
				})
				.catch( err => {
					console.log(err.message)
					Vue.swal({
						type: 'error',
						title: 'Ops!',
						text: 'Algo deu errado! Confira seus dados'
					});
				});
	},

	logout(){
		firebase.auth().signOut()
				.then(()=>{
					Vue.swal({
						toast: true,
						position: 'bottom-end',
						showConfirmButton: false,
  						timer: 4000,
  						type: 'success',
  						title: 'Logout efetuado com sucesso'
					})
					router.push({"name": "Home"})
				})
	},

	register({commit}, payload){
		firebase.database().ref().child('users').push({
		    username: payload.name,
		    email: payload.email,
		    phone: payload.phone
		})
		.then(()=>{
			Vue.swal({
				type: 'success',
				title: 'Obrigado',
				text: 'Cadastro efetuado com sucesso'
			});

			router.push({"name": "Home"})
		})
		.catch(err=>{
			console.log(err)
			Vue.swal({
				type: 'error',
				title: 'Ops!',
				text: 'Algo deu errado! Confira seus dados'
			});
		});
	}
  }
})
