import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Signup from './views/Signup.vue'
import About from './views/About.vue'
import List from './views/List.vue'
import firebase from 'firebase'
import VueSweetalert2 from 'vue-sweetalert2';

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },

    {
      path: '/sobre',
      name: 'About',
      component: About
    },

    {
      path: '/login',
      name: 'Login',
      component: Login
    },

    {
      path: '/cadastro',
      name: 'Signup',
      component: Signup
    },

    {
      path: '/listagem',
      name: 'List',
      component: List,
      meta: {
        requiresAuth: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if(to.matched.some(rec => rec.meta.requiresAuth)){
    let user = firebase.auth().currentUser

    if(user){
      next()
    }else{
      next({name: 'Login'})
     Vue.swal({
        type: 'error',
        title: 'Ops!',
        text: 'Área restrita! Efetue login'
    });
    }
  }else{
    next()
  }
})

export default router;